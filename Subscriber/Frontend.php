<?php

namespace Shopware\SegMinCustAge\Subscriber;

/**
 * @SuppressWarnings(PHPMD.CamelCaseClassName)
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class Frontend implements \Enlight\Event\SubscriberInterface
{
    protected $_minAge;

    protected $_snippets;

    public function __construct(int $minAge)
    {
        $this->_minAge = $minAge;
    }

    public static function getSubscribedEvents()
    {
        return array(
            'Shopware_Controllers_Frontend_Register_validatePersonal_FilterRules'
            => 'onValidatePersonal',
            'Shopware_Controllers_Frontend_Register::validatePersonal::after'
            => 'afterValidatePersonal'
        );
    }

    /**
     * Complain if customer isn't old enough
     */
    public function afterValidatePersonal(Enlight_Hook_HookArgs $arguments)
    {
        $retval = $arguments->getReturn();
        $errorMessages = $retval['sErrorMessages'];
        $errorFlag = $retval['sErrorFlag'];

        $controller = $arguments->getSubject();
        $personal = $controller->Request()->getParam('register')['personal'];

        $birthdayString = sprintf(
            "%04d-%02d-%02d",
            (int)$personal["birthyear"],
            (int)$personal["birthmonth"],
            (int)$personal["birthday"]
        );
        $birthday = new \DateTime($birthdayString, new \DateTimezone("UTC"));

        if (!$this->isCustomerOldEnough($birthday)) {
            $errorMessages[] = $this->getSnippet('customertooyoung');
            $errorFlag['birthday'] = true;
        }

        $arguments->setReturn(
            ["sErrorMessages" => $errorMessages, "sErrorFlag" => $errorFlag]
        );
    }


    /**
     * Mark birthdate required fields
     */
    public function onValidatePersonal(Enlight_Event_EventArgs $arguments)
    {
        $shopConfig = new \Shopware_Components_Config(
            array(
                'shop' => Shopware()->Models()->find(
                    "Shopware\\Models\\Shop\\Shop",
                    Shopware()->Shop()->getId()
                ),
                'db' => Shopware()->Db()
            )
        );
        if (isset($shopConfig->requirebirthdayfield)) {
            if ($shopConfig->requirebirthdayfield) {
                return;
            }
            Shopware()->Pluginlogger()->Info(
                "Shopware is set not to require birthday input "
                . "but this plugin is enabled. Make up your mind."
            );
        }

        $rules = $arguments->getReturn();

        $required = array('required' => 1);
        $rules["birthyear"] = $required;
        $rules["birthmonth"] = $required;
        $rules["birthday"] = $required;

        return $rules;
    }

    /**
     * Query the snippetmanager.
     */
    private function getSnippet($name)
    {
        if (!isset($this->_snippets)) {
            $this->_snippets = Shopware()->_snippets()
                ->getNamespace('frontend/segmincustage/main');
        }
        return $this->_snippets->get($name);
    }

    /**
     * Test if DateTime is more than minAge years ago
     */
    private function isCustomerOldEnough(\DateTime $birthday)
    {
        if ($this->_minAge <= 0) {
            Shopware()->Pluginlogger()->Info(
                "Ignoring invalid customer minimum age setting: "
                . $this->_minAge
            );
            return true;
        }
        $minAgeBirthday = (new \DateTime("now", $birthday->getTimezone()))->sub(
            new \DateInterval("P" . $this->_minAge . "Y")
        );
        return ($birthday <= $minAgeBirthday);
    }
}
