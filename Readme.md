SegMinCustAge
=============

What this is
------------

A Shopware-Plugin to require a minimum age on customer account creation. When a customer enters a birthdate that is below the minimum threshold, they are presented with a friendly error message.

What this isn't
---------------

It doesn't help with verifying that the customer did in fact give their genuine birthdate.

What to keep in mind
--------------------

The plugin assumes that you want to keep out underage customers. If you have a different reason, you can edit the error message within the Shopware [snippet administration](https://developers.shopware.com/designers-guide/snippets/) panel (search for `customertooyoung`).

Compatibility
-------------

The code is tested to run with Shopware versions 4.3 and 5.0. It requires PHP 5.4 or higher.

Where to get it
---------------

Find the git-repository at:
https://bitbucket.org/s-eilt/segmincustage.git

Install
-------

To install the plugin, simply clone it Shopware's plugin directory:

    cd engine/Shopware/Plugins/Local/Frontend
    git clone https://bitbucket.org/s-eilt/segmincustage.git \
        SegMinCustAge

Alternatively, grab the zip file and upload it with the plugin manager.

Licence
-------

See LICENSE file.

Author
------

Simon Eilting <simon@eilting.net>
