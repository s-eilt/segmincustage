<?php

/**
 * @SuppressWarnings(PHPMD.CamelCaseClassName)
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class Shopware_Plugins_Frontend_SegMinCustAge_Bootstrap
    extends Shopware_Components_Plugin_Bootstrap
{
    const SUPPORT = "simon@eilting.net";

    protected $_pluginInfo;

    public function __construct($name, $info=null)
    {
        $this->_pluginInfo = json_decode(
            file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'plugin.json'),
            true
        ); 
        parent::__construct($name, $info);
    }

    public function getCapabilities()
    {
        return array(
            'install' => true,
            'update' => true,
            'enable' => true
        );
    }
    
    public function getVersion()
    {
        return $this->_pluginInfo['currentVersion'];
    }

    public function getLabel()
    {
        return $this->_pluginInfo['label']['en'];
    }

    public function getInfo()
    {
        return array_merge(
            $this->_pluginInfo,
            array(
                'label' => $this->getLabel(),
                'support' => static::SUPPORT,
                'version' => $this->getVersion()
            )
        );
    }

    public function install()
    {
        $minVersion = $this->_pluginInfo['compatibility']['minimumVersion'];
        if (!$this->assertVersionGreaterThen($minVersion)) {
            throw new \RuntimeException(
                'At least Shopware ' . $minVersion . ' is required'
            );
        }

        $this->createForm();

        $this->subscribeEvent(
            'Enlight_Controller_Front_DispatchLoopStartup',
            'onStartDispatch'
        );

        $this->update('1.0.0');

        return array('success' => true, 'invalidateCache' => array('frontend'));
    }

    /** @SuppressWarnings(PHPMD.UnusedFormalParameter) */
    public function onStartDispatch(Enlight_Event_EventArgs $args)
    {
        $this->Application()->Loader()
            ->registerNamespace('Shopware\SegMinCustAge', $this->Path());

        $minAge = (int)$this->Config()->get('minAge');

        $this->Application()->Events()->addSubscriber(
            new \Shopware\SegMinCustAge\Subscriber\Frontend($minAge)
        );
    }

    public function uninstall()
    {
        return true;
    }

    /** @SuppressWarnings(PHPMD.UnusedFormalParameter) */
    public function update($oldversion)
    {
        switch ($oldversion) {
        default:
            Shopware()->Pluginlogger()->Error(
                'Update from version ' . $oldversion . ' not implemented.'
            );
            return false;
        case '1.0.0':
            //nop
            //no break;
        }
        return true;
    }

    private function createForm()
    {
        $localeRepository = Shopware()->Models()
            ->getRepository('\Shopware\Models\Shop\Locale');

        $form = $this->Form();
        $form->setElement(
            'number', 'minAge',
            array(
                'label' => "New customer's minimum age",
                'minValue' => 16,
                'value' => 18,
                'scope' => Shopware\Models\Config\Element::SCOPE_SHOP,
                'required' => true
            )
        );
        $translations = array(
            'de_DE' => array(
                'minAge' => "Mindestalter f&uuml;r Neukunden",
            ),
        );
        foreach ($translations as $locale => $snippets) {
            $localeModel = $localeRepository->findOneBy(['locale' => $locale]);
            if ($localeModel === null) {
                continue;
            }    
            foreach ($snippets as $element => $snippet) {

                $elementModel = $form->getElement($element);

                if ($elementModel === null) {
                    continue;
                }
                $translationModel =
                    new \Shopware\Models\Config\ElementTranslation();
                $translationModel->setLabel($snippet);
                $translationModel->setLocale($localeModel);

                $elementModel->addTranslation($translationModel);
            }
        }
    }
}
